<?php

namespace Drupal\migrate_source_jcr\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Jackalope\RepositoryFactoryJackrabbit;
use PHPCR\PathNotFoundException;
use PHPCR\SimpleCredentials;
use PHPCR\UnsupportedRepositoryOperationException;


/**
 * Source plugin for importing JCR data.
 *
 * @MigrateSource(
 *  id = "jcr"
 * )
 */
class Jcr extends SourcePluginBase {

  /**
   * Information on the source fields to be extracted from the data.
   *
   * @var array[]
   *   Array of field information keyed by field names. A 'label' subkey
   *   describes the field for migration tools; a 'path' subkey provides the
   *   source-specific path for obtaining the value.
   */
  protected $fields = [];

  /**
   * The Jackrabbit repository factory.
   *
   * @var \Jackalope\RepositoryFactoryJackrabbit
   */
  protected $jackalopeFactory;

  /**
   * The Jackrabbit URL.
   *
   * @var string
   */
  protected $host;

  /**
   * The query.
   *
   * @var \PHPCR\Query\QueryInterface
   */
  protected $query;

  /**
   * The query string.
   *
   * @var string
   */
  protected $queryString;

  /**
   * The query type.
   *
   * @var string
   */
  protected $queryType;

  /**
   * The CRX username.
   *
   * @var string
   */
  protected $user = 'admin';

  /**
   * The CRX password.
   *
   * @var string
   */
  protected $pass = 'admin';

  /**
   * The CRX workspace.
   *
   * @var string
   */
  protected $workspace = 'crx.default';

  /**
   * The batch size.
   *
   * @var int
   */
  protected $batchSize = 0;

  /**
   * The current batch.
   *
   * @var int
   */
  protected $batch = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->fields = $configuration['fields'];
    $this->jackalopeFactory = new RepositoryFactoryJackrabbit();
    $this->host = $configuration['host'];
    $this->queryString = $configuration['query'];
    $this->queryType = $configuration['type'];
    $this->user = $configuration['user'];
    $this->pass = $configuration['pass'];
    $this->workspace = $configuration['workspace'];
    $this->batchSize = $configuration['batch_size'];

  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return "JCR data";
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {

    if ($this->batch == 0) {
      $this->buildQuery();
    }

    if ($this->batchSize > 0) {
      $offset = $this->batch * $this->batchSize;
      $this->query->setOffset($offset);
    }

    $queryResult = $this->query->execute();

    $nodesArray = [];
    foreach ($queryResult->getNodes() as $path => $node) {
      $nodesArray[] = ['node' => $node, 'path_hash' => md5($path)];
    }

    $nodesObject = new \ArrayObject($nodesArray);

    return $nodesObject->getIterator();
  }

  /**
   * Position the iterator to the following row.
   */
  protected function fetchNextRow() {
    $this->getIterator()->next();
    // Line 156 is moving to the next position in the iterator,
    // even if there aren’t any more elements left in it.
    // The valid check makes sure the position in the iterator is valid.
    // In this case, if the position in the iterator is not valid,
    // that signifies that we’ve exhausted the batch and there’s
    // no longer anything to iterate over. So we go get the next batch.
    if ($this->batchSize > 0 && !$this->getIterator()->valid()) {
      $this->fetchNextBatch();
    }
  }

  /**
   * Prepares query for the next set of data from the source database.
   */
  protected function fetchNextBatch() {
    $this->batch++;
    // Unset the iterator so that getIteratator uses the new query, @see getIterator()
    unset($this->iterator);
    $this->getIterator()->rewind();
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    /** @var \Jackalope\Node $node */
    $node = $row->getSourceProperty('node');

    foreach ($this->fields as $field_info) {
      try {
        /** @var \Jackalope\Node $propertyElement */
        $propertyElement = ('' === $field_info['subpath']) ? $node : $node->getNode($field_info['subpath']);
        $value = $propertyElement->getPropertyValue($field_info['property']);

        $row->setSourceProperty($field_info['name'], $value);

      }
      catch (PathNotFoundException $e) {
        $row->setSourceProperty($field_info['name'], NULL);

      }
      catch (UnsupportedRepositoryOperationException $e) {
        $row->setSourceProperty($field_info['name'], NULL);
      }
      catch (\Exception $e) {
        throw new MigrateException($e->getMessage());
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['path_hash']['type'] = 'string';
    return $ids;
  }

  /**
   * Builds the query with parameters defined in the migration configuration.
   */
  protected function buildQuery() {
    $repository = $this->jackalopeFactory->getRepository(
      ["jackalope.jackrabbit_uri" => $this->host]
    );

    $credentials = new SimpleCredentials($this->user, $this->pass);
    $session = $repository->login($credentials, $this->workspace);
    $workspace = $session->getWorkspace();
    $queryManager = $workspace->getQueryManager();
    $query = $queryManager->createQuery($this->queryString, $this->queryType);
    if ($this->batchSize > 0) {
      $query->setLimit($this->batchSize);
    }

    $this->query = $query;
  }

}
